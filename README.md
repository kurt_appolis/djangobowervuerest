# README #

A Django 2.# project using bower, VueJS, and Django REST framework.


### How do I get set up? ###

> cd ./djangobowervuerest

> virtualenv -p python3 ./env


> source ./env/bin/activate


> pip install -r ./requirements.txt


> cd ./dummyproject


> ./manage.py migrate


> ./manage.py createsuperuser


> ./manage.py bower install vue


> ./manage.py bower install vue-resource


> ./manage.py collectstatic 


> ./manage.py runserver


### Resources ###

* https://github.com/nvbn/django-bower

* https://vuejs.org/

* https://github.com/pagekit/vue-resource

* http://www.django-rest-framework.org/
