"""Base model classes."""

from django.db import models


class ListItem(models.Model):
    """List Item in groceryList."""

    text = models.CharField(max_length=20, null=True, blank=True)

    def __str__(self):
        """To string function."""
        return self.text
