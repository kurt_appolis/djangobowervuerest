Vue.component('todo-item', {
  props: ['todo'],
  template: '<li>{{ todo.text }}</li>'
})

var app = new Vue({
  delimiters: ["[[", "]]"],
  el: '#app',
  data: {
    groceryList: [],
    newItem: {}
  },
  created: function(){
    this.getList();
  },
  methods: {
    getList: function(){
      this.$http.get('/api/list/').then(function(response) {
        this.groceryList = response.body;
      }, function(response){
      });
    }, 
    createItem: function(){
      this.$http.post('/api/list/', {'text': this.newItem.title}).then(function(response) {
        console.log(response.body);
        this.getList();
      }, function(response){
      });
    }
  }
})