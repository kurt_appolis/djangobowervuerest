"""Base model admin views."""
from django.contrib import admin
from .models import ListItem


@admin.register(ListItem)
class ListItemAdmin(admin.ModelAdmin):
    """ListItemAdmin admin view class."""

    list_display = ('id', 'text')
