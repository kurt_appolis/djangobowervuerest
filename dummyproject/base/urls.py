"""Base url configurations."""
from django.urls import path, include
from .views import (LandingPageTemplateView)

app_name = "base"

urlpatterns = [
    path('', LandingPageTemplateView.as_view(), name="index"),
    # api urls
    path('api/', include('base.api.urls'), name="base-api"),
]
