"""Base views."""
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from rest_framework.authtoken.models import Token
from django.views.generic import (TemplateView)


class LandingPageTemplateView(TemplateView):
    """Landing Page TemplateView."""

    template_name = "base/index.html"

    def get_context_data(self, **kwargs):
        """Default context function."""
        context = super(LandingPageTemplateView, self).get_context_data(**kwargs)
        token, user = Token.objects.get_or_create(user=self.request.user)
        context['token'] = token
        return context

    @method_decorator(login_required(login_url='admin:logout'))
    def dispatch(self, *args, **kwargs):
        """Class dispatch method."""
        return super(LandingPageTemplateView, self).dispatch(*args, **kwargs)
