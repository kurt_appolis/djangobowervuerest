"""Base API url configurations."""
from django.urls import path

from .views import (ListItemListView)

urlpatterns = [
    path('list/', ListItemListView.as_view(), name="api-list-listitem"),
]
