"""Base api views."""
from rest_framework import generics
from rest_framework import mixins
from rest_framework import status
from rest_framework.parsers import MultiPartParser, FormParser, JSONParser
from rest_framework.response import Response

from .serializers import (ListItemSerializer)
from base.models import (ListItem)


class ListItemListView(mixins.ListModelMixin, generics.GenericAPIView):
    """ListItemSerializer List View class."""

    serializer_class = ListItemSerializer
    parser_classes = (MultiPartParser, FormParser, JSONParser)

    def get(self, request, *args, **kwargs):
        """Retrieve all list items."""
        self.queryset = ListItem.objects.all().order_by('-pk')

        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        """Create a new list item."""
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            try:
                serializer.save()
                return Response({'success': serializer.data}, status=status.HTTP_201_CREATED)
                pass
            except Exception as e:
                return Response({'error': e}, status=status.HTTP_417_EXPECTATION_FAILED)
        else:
            return Response({'error': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)
