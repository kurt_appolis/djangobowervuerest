"""Question api serializers."""
from base.models import (ListItem)

from rest_framework import serializers


class ListItemSerializer(serializers.ModelSerializer):
    """ListItem Serializer."""

    class Meta:
        """ListItem Meta class."""

        model = ListItem
        fields = ('id', 'text')
